const SiteData = {
    "media_url" : process.env.REACT_APP_MEDIA_URL,
    "site_title": "Ryan Dingle",
    "site_url": "/",
    "site_year": "2019",
    "site_domain": "www.ryandingle.dev",
    "site_contact": "N/A",
    "site_email": "ryandingle09@gmail.com",
    "site_address": "Somewhere in Australia",
    "header_title": "I'm a herooo! .",
    "header_description": "Light Turner, a bright student, stumbles across a mystical notebook that has the power to kill any person whose name he writes in it Light decides to launch a secret crusade to rid the streets of criminals."
};

export default SiteData
import React from 'react';
import { LightSpeed, Fade } from 'react-reveal';
import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import Preloader from '../../global/preloader';
import API from '../../global/api';
import META from '../../global/meta';

class About extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            site: [this.props.site][0],
            data: [],
            about: [],
            skill: [],
            isLoading: true,
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this.getData();
    }

    getData() {
        API.get('history')
            .then(res => this.setState({
                    data: res.data,
                    isLoading: false,
                })
            )
            .catch(error => this.setState({
                    isLoading: false
                })
            )
            .catch(error => {
                alert('Something went wrong. Please try again later.');
                console.log(error);
            });

        API.get('aboutInfo')
            .then(res => this.setState({
                    about: res.data,
                    isLoading: false,
                })
            )
            .catch(error => this.setState({
                    isLoading: false
                })
            )
            .catch(error => {
                alert('Something went wrong. Please try again later.');
                console.log(error);
            });

        API.get('skill')
            .then(res => this.setState({
                    skill: res.data,
                    isLoading: false,
                })
            )
            .catch(error => this.setState({
                    isLoading: false
                })
            )
            .catch(error => {
                alert('Something went wrong. Please try again later.');
                console.log(error);
            });
    }

    render() {
        return (
            <section className="about-area section_gap gray-bg">
                {
                    this.state.isLoading ?
                        <Preloader />
                    :
                    <div className="container">
                        <div className="row align-items-center justify-content-between">

                        {
                            this.state.about.length === 0  ? 
                            <LightSpeed left>
                                <div className="col-lg-5 about-left">
                                    <img className="img-fluid" src="img/about-img.png" alt="" />
                                </div>
                            </LightSpeed>
                            :
                            this.state.about.map(item => 
                                <LightSpeed left>
                                    <div className="col-lg-5 about-left">
                                        <img className="img-fluid" src={this.state.site.media_url+``+ item.fields.image } alt={item.heading} />
                                    </div>
                                </LightSpeed>
                            )
                        }

                        {
                            this.state.about.length === 0  ? 
                                <LightSpeed right>
                                    <div className="col-lg-6 col-md-12 about-right">
                                        <div className="main-title text-left">
                                            <h1>about myself</h1>
                                        </div>
                                        <div className="mb-50 wow fadeIn" data-wow-duration=".8s">
                                            <p>
                                            inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards
                                            especially in the
                                            workplace. That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate
                                            behavior
                                            is often laughed. inappropriate behavior is often laughed off as “boys will be boys,” women face higher.
                                            </p>
                                            <p>That’s why it’s crucial that, as women, our behavior on the job is beyond reproach. inappropriate behavior is
                                            often
                                            laughed.
                                            </p>
                                        </div>
                                    </div>
                                </LightSpeed>
                            : 
                            this.state.about.map(item => 
                                <LightSpeed right>
                                    <META item_description={item.fields.introduction} item_image={this.state.site.media_url+ `` + item.fields.image} />
                                    <div className="col-lg-6 col-md-12 about-right">
                                        <div className="main-title text-left">
                                            <h1>{item.fields.heading}</h1>
                                        </div>
                                        <div className="mb-50 wow fadeIn" data-wow-duration=".8s">
                                            <p>{item.fields.introduction}</p>
                                        </div>
                                    </div>
                                </LightSpeed>
                            )
                        }
                            
                        </div>
                        
                        <hr />

                        <h1 className="text-center mt-s2 mb-s2">History</h1>

                        <VerticalTimeline>
                            {
                                this.state.data.map(item => 
                                    <VerticalTimelineElement
                                        className="vertical-timeline-element--work"
                                        date={item.fields.start +` - `+ item.fields.end}
                                        iconStyle={{ background: 'rgb(33, 150, 243)', color: '#fff' }}
                                    >
                                        <h3 className="vertical-timeline-element-title">{item.fields.job_title}</h3>
                                        <p>{item.fields.description}</p>
                                    </VerticalTimelineElement>
                                )
                            }
                            <VerticalTimelineElement
                                iconStyle={{ background: 'rgb(16, 204, 82)', color: '#fff' }}
                            />
                        </VerticalTimeline>
                        
                        <hr />
                        
                        {
                            this.state.about.length === 0  ? 
                            <h1 className="text-center mt-s2 mb-s2">Technologies</h1>
                            :
                            this.state.about.map(item => 
                                <h1 className="text-center mt-s2 mb-s2">{item.fields.heading_bottom}</h1>
                            )
                        }
                        
                        {
                            this.state.skill.length === 0  ? 
                            <div className="mt-s2">
                                <div className="row">
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                    <div className="col-md-2 mb-s2">
                                        <Fade><img className="img-fluid" alt="" src="/img/web-app.png" /></Fade>
                                    </div>
                                </div>
                            </div>
                            :
                                <div className="mt-s2">
                                    <div className="row">
                                        {
                                            this.state.skill.map(item => 
                                                <div className="col-md-2 mb-s2">
                                                    <Fade><img className="img-fluid" src={this.state.site.media_url+``+ item.fields.image } alt={item.fields.title} style={{width: '160px', height: '160px'}} /></Fade>
                                                </div>
                                            )
                                        } 
                                    </div>
                                </div>

                        }
                    </div>
                }
            </section>
        );
    }
}

export default About;
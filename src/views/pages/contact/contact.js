import React from 'react';
import { Fade, LightSpeed } from 'react-reveal';
import Preloader from '../../global/preloader';
import API from '../../global/api';
import FormContact from './form';
import META from '../../global/meta';

class Contact extends React.Component{

    constructor(props) {
        super(props);

        this.state = {
            site: [this.props.site][0],
            contact: [],
            status: {},
            isLoading: true,
        }
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this.getData();
    }

    getData() {
        API.get('contactInfo')
            .then(res => this.setState({
                    contact: res.data,
                    isLoading: false,
                })
            )
            .catch(error => this.setState({
                    isLoading: false
                })
            )
            .catch(error => {
                alert('Something went wrong. Please try again later.');
                console.log(error);
            });
    }

    render() {
        return (
            <section className="contact_area section_gap">
                <div className="container">
                    {
                        this.state.isLoading ?
                            <Preloader />
                        :
                        this.state.contact.length === 0 ?
                            <div className="row">
                                <div className="col-lg-3">
                                    <LightSpeed left>
                                        <div className="contact_info">
                                            <div className="info_item">
                                            <i className="lnr lnr-home" />
                                            <h6>California, United States</h6>
                                            <p>Santa monica bullevard</p>
                                            </div>
                                            <div className="info_item">
                                            <i className="lnr lnr-phone-handset" />
                                            <h6><a href="/">00 (440) 9865 562</a></h6>
                                            <p>Mon to Fri 9am to 6 pm</p>
                                            </div>
                                            <div className="info_item">
                                            <i className="lnr lnr-envelope" />
                                            <h6><a href="/">me@rldwebshop.com</a></h6>
                                            <p>Send us your query anytime!</p>
                                            </div>
                                        </div>
                                    </LightSpeed>
                                </div>
                                <div className="col-lg-9 mb-s">
                                    <FormContact />
                                </div>
                            </div>
                        :
                            this.state.contact.map(item => 
                                <div className="row">
                                    <META item_description={item.fields.description} title="Ryan Dingle | Contact" item_title="Ryan Dingle | Contact" />
                                    <div className="col-lg-12">
                                        <LightSpeed left><h3>Contact Me</h3></LightSpeed>
                                        <br />
                                        <Fade right><p>{item.fields.description}</p></Fade>
                                        <br />
                                        <br />
                                    </div>
                                    <hr />
                                    <div className="col-lg-3">
                                        <LightSpeed left>
                                            <div className="contact_info">
                                                <div className="info_item">
                                                <i className="lnr lnr-home" />
                                                <h6>{item.fields.address}</h6>
                                                <p>&nbsp;</p>
                                                </div>
                                                <div className="info_item">
                                                <i className="lnr lnr-phone-handset" />
                                                <h6><a href="/">{item.fields.contact}</a></h6>
                                                <p>Mon to Fri 8am to 10pm</p>
                                                </div>
                                                <div className="info_item">
                                                <i className="lnr lnr-envelope" />
                                                <h6><a href="/">{item.fields.email}</a></h6>
                                                <p>Send us your query anytime!</p>
                                                </div>
                                            </div>
                                        </LightSpeed>
                                    </div>
                                    <div className="col-lg-9 mb-s">
                                        <FormContact />
                                    </div>
                                </div>
                            )
                    }
                </div>
            </section>
        );
    }
}

export default Contact;
import React, { useState } from 'react';
import { Fade, LightSpeed } from 'react-reveal';
import { useForm } from 'react-hook-form';
import API from '../../global/api';
import CSRFToken from '../../global/csrf';
import Preloader from '../../global/preloader';
import { toast } from 'react-toastify';

export default function FormContact() {
    const { register, handleSubmit, errors } = useForm();
    
    const email  = errors['email'] ? 'is-invalid' : '';
    const name  = errors['name'] ? 'is-invalid' : '';
    const message  = errors['message'] ? 'is-invalid' : '';

    const [isLoading, setLoading] = useState(false);

    const onSubmit = (data, e) => {

        let formData = new FormData();

        formData.append('name', data['name']);
        formData.append('contact', data['contact']);
        formData.append('email', data['email']);
        formData.append('message', data['message']);

        setLoading(true);

        API.post('contact-message', formData)
            .then(res => {
                    //setStatus(res.data);
                    setLoading(false);
                    e.target.reset();
                    toast.success("Message Sent.");
                }
            )
            .catch(error => {
                    //setStatus(error.data);
                    setLoading(false);
                    toast.error("Something went wrong. Please try again later.");
                }
            )
            .catch(error => {
                alert('Something went wrong. Please try again later.');
                console.log(error);
                toast.error("Something went wrong. Please try again later.");
            });
    } 

    return (
        <form className="row contact_form" onSubmit={handleSubmit(onSubmit)} method="post" id="contactForm" noValidate="novalidate">
            {
               isLoading ? <div className="col-md-12"><Preloader /></div> : ''
            }
            <CSRFToken />
            <Fade top>
                <div className="col-md-6">
                    <div className="form-group">
                        <label>Your Name</label>
                        <input type="text" className={`form-control `+  name } placeholder="Name" name="name" ref={register({required: true, maxLength: 80})} />
                    </div>
                    <br />
                    <div className="form-group">
                        <label>Email Address</label>
                        <input type="text" className={`form-control `+  email }  placeholder="Email" name="email" ref={register({required: true, pattern: /^\S+@\S+$/i})} />
                    </div>
                    <br />
                    <div className="form-group">
                        <label>Contact</label>
                        <input type="text" className="form-control"  placeholder="Contact" name="contact" ref={register} />
                    </div>
                </div>
            </Fade>
            <LightSpeed right>
                <div className="col-md-6">
                    <div className="form-group">
                        <label>Message</label>
                        <textarea name="message" className={`form-control `+  message }  ref={register({required: true})} placeholder="Enter Message" />
                    </div>
                </div>
                <div className="col-md-12 text-right">
                    <button type="submit" value="submit" className="primary-btn"><span>Send Message</span></button>
                </div>
            </LightSpeed>
        </form>
    )
}
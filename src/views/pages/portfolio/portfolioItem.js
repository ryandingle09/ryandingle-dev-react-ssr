import React from 'react';
import { LightSpeed, Fade } from 'react-reveal';
import ReactHtmlParser from 'react-html-parser';
import Rotate from 'react-reveal/Rotate';
import Preloader from '../../global/preloader';
import API from '../../global/api';
import NotFound from '../404';
import {
    FacebookIcon,
    FacebookShareButton,
    TwitterIcon,
    TwitterShareButton,
    LinkedinShareButton,
    LinkedinIcon,
    PinterestShareButton,
    PinterestIcon,
    EmailShareButton,
    EmailIcon,
    RedditIcon,
    RedditShareButton
  } from "react-share";
import META from '../../global/meta';

class PortfolioItem extends React.Component {

    constructor(props) {
        super(props);

        const { match: { params } } = this.props;
    
        this.state = {
            isLoading: true,
            site: [this.props.site][0],
            isHovering: false,
            isItemHover: 0,
            item: params.id,
            work: []
        };
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this.getData();
    }

    getData() {
        API.get('work?item='+this.state.item)
            .then(res => this.setState({
                    work: res.data.data,
                    pages: res.data.pages,
                    isLoading: false,
                })
            )
            .catch(error => this.setState({
                    isLoading: false
                })
            )
            .catch(error => {
                alert('Something went wrong. Please try again later.');
                console.log(error);
            });
    }

    render() {
        return (
            <section class="portfolio_details_area section_gap">
                <div class="container">
                    {
                        this.state.isLoading ?
                        <Preloader />
                        :
                            this.state.work.length === 0 ?
                                <NotFound />
                            :
                            this.state.work.map(item => 
                                <div class="portfolio_details_inner">
                                    <META item_description={item.fields.description} item_image={this.state.site.media_url+ `` + item.fields.cover_image} item_title={`Ryan Dingle > Portfolio > ` + item.fields.title} title={`Ryan Dingle > Portfolio > ` + item.fields.title} />
                                    <div class="row">
                                        <Rotate>
                                            <div class="col-md-6">
                                                <div class="left_img">
                                                    <img class="img-fluid" src={this.state.site.media_url+``+item.fields.cover_image} alt={item.fields.title} style={{width: '555px', height: '471px'}} />
                                                </div>
                                            </div>
                                        </Rotate>

                                        <div class="offset-md-1 col-md-5">
                                            <LightSpeed  right>
                                                <div class="portfolio_right_text mt-30">
                                                    <h4>{item.fields.title}</h4>
                                                    <ul class="list">
                                                        <li><span>Client</span>: {item.fields.client}</li>
                                                        <li>
                                                            <span>Website</span>: &nbsp;
                                                            {
                                                                item.fields.link ? 
                                                                <a href={item.fields.link}>{item.fields.link}</a>
                                                                : 'N/A'
                                                            }
                                                        </li>
                                                        <li><span>Completed</span>: {item.fields.date}</li>
                                                    </ul>
                                                    <ul class="list social_details">
                                                        <li>
                                                            <div className="Demo__some-network">
                                                                <FacebookShareButton
                                                                    url={window.location.href}
                                                                    quote={item.fields.title}
                                                                    className="Demo__some-network__share-button"
                                                                >
                                                                    <FacebookIcon size={32} round />
                                                                </FacebookShareButton>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div className="Demo__some-network">
                                                                <TwitterShareButton
                                                                    url={window.location.href}
                                                                    quote={item.fields.title}
                                                                    className="Demo__some-network__share-button"
                                                                >
                                                                    <TwitterIcon size={32} round />
                                                                </TwitterShareButton>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div className="Demo__some-network">
                                                                <LinkedinShareButton
                                                                    url={window.location.href}
                                                                    quote={item.fields.title}
                                                                    className="Demo__some-network__share-button"
                                                                >
                                                                    <LinkedinIcon size={32} round />
                                                                </LinkedinShareButton>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div className="Demo__some-network">
                                                                <RedditShareButton
                                                                    url={window.location.href}
                                                                    quote={item.fields.title}
                                                                    className="Demo__some-network__share-button"
                                                                >
                                                                    <RedditIcon size={32} round />
                                                                </RedditShareButton>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div className="Demo__some-network">
                                                                <PinterestShareButton
                                                                    url={window.location.href}
                                                                    quote={item.fields.title}
                                                                    className="Demo__some-network__share-button"
                                                                >
                                                                    <PinterestIcon size={32} round />
                                                                </PinterestShareButton>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div className="Demo__some-network">
                                                                <EmailShareButton
                                                                    url={window.location.href}
                                                                    quote={item.fields.title}
                                                                    className="Demo__some-network__share-button"
                                                                >
                                                                    <EmailIcon size={32} round />
                                                                </EmailShareButton>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </LightSpeed>
                                        </div>
                                    </div>
                                    <Fade bottom>
                                        <div className="row">{ ReactHtmlParser (item.fields.description) }</div>
                                    </Fade>
                                </div>
                            )
                    }
                </div>
            </section>
        );
    }
}

export default PortfolioItem;
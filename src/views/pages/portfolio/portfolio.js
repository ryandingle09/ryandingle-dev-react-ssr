import React from 'react';
import { Link } from "react-router-dom";
import { Zoom, Flip, LightSpeed, Fade } from 'react-reveal';
import Preloader from '../../global/preloader';
import API from '../../global/api';
import BlogPagination from '../../global/pagination';
import META from '../../global/meta';

class Portfolio extends React.Component {

    constructor(props) {
        super(props);
    
        this.handleMouseHover = this.handleMouseHover.bind(this);
    
        this.state = {
            isLoading: true,
            site: [this.props.site][0],
            work: [],
            isHovering: false,
            isItemHover: 0,
            search: this.props.location.search ? this.props.location.search : '',
            pages: 0
        };
    }

    handleMouseHover(e) {
        this.setState(this.toggleHoverState);
        this.setState({'isItemHover':e.currentTarget.dataset.item});
    }

    toggleHoverState(state) {
        return {
            isHovering: !state.isHovering,
        };
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this.getData();
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        window.scrollTo(0, 0);

        this.setState({
            isLoading:true,
            search: nextProps.location.search
        });

        this.getData(nextProps.location.search);
    }

    getData(search='') {
        API.get('work'+search)
            .then(res => this.setState({
                    work: res.data.data,
                    pages: res.data.pages,
                    isLoading: false,
                })
            )
            .catch(error => this.setState({
                    isLoading: false
                })
            )
            .catch(error => {
                alert('Something went wrong. Please try again later.');
                console.log(error);
            });
    }

    render() {

        const query = new URLSearchParams(this.props.location.search);
        const active = query.get('page') ? query.get('page') : 1;

        return (
            <section className="section_gap portfolio_area" id="work">

            <META item_description="Ryan Dingle Portfolio List" item_image={window.location.protocol+ `://`+ window.location.host +`/img/web-app.png`} item_title="Ryan Dingle | Portfolio" title="Ryan Dingle | Portfolio" />
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-6 text-center">
                        <div className="main-title">
                            <LightSpeed><h1>Portfolio</h1></LightSpeed>
                            <Fade><p>Check some of my works</p></Fade>
                        </div>
                        </div>
                    </div>
                    
                    <div className="row justify-content-center">
                        {
                            this.state.isLoading ?
                                <Preloader />
                            :
                                this.state.work.length === 0 ?
                                <div className="card-columns">

                                    <Zoom>
                                        <div className="card bg-light text-white items" data-item="1" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                                            <Link to="/portfolio/item" className="plain-link2">
                                                <img src="/img/saitama.jpg" className="card-img" alt="/img/saitama.jpg" style={{width: '366px', height: '366px'}} />
                                                <div className="card-img-overlay text-center">
                                                    {
                                                    this.state.isHovering && this.state.isItemHover === 1 
                                                    ?
                                                    <div>
                                                        <Flip right cascade when={this.state.toggleHoverState}>
                                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3> 
                                                        </Flip>
                                                        <Flip bottom cascade when={this.state.show}>
                                                        <Link to="/portfolio/item" className="primary-btn white-color white-shadow">View</Link>
                                                        </Flip>
                                                    </div>
                                                    : ''
                                                    }
                                                </div>
                                            </Link>
                                        </div>
                                    </Zoom>
                                    
                                    <Zoom delay={200}>
                                        <div className="card bg-light text-white items" data-item="2" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                                            <Link to="/portfolio/item" className="plain-link2">
                                                <img src="/img/bg1.jpg" className="card-img-top" alt="..."  style={{width: '366px', height: '366px'}} />
                                                <div className="card-img-overlay text-center"> 
                                                    {
                                                    this.state.isHovering && this.state.isItemHover === 2 
                                                    ?
                                                    <div>
                                                        <Flip right cascade when={this.state.toggleHoverState}>
                                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3> 
                                                        </Flip>
                                                        <Flip bottom cascade when={this.state.show}>
                                                        <Link to="/portfolio/item" className="primary-btn white-color  white-shadow">View</Link>
                                                        </Flip>
                                                    </div>
                                                    : ''
                                                    }
                                                </div>
                                            </Link>
                                        </div>
                                    </Zoom>
                                    
                                    <Zoom delay={400}>
                                        <div className="card bg-light text-white items" data-item="3" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                                            <Link to="/portfolio/item" className="plain-link2">
                                                <img src="/img/bg3.jpg" className="card-img-top" alt="..."  style={{width: '366px', height: '366px'}} />
                                                <div className="card-img-overlay text-center">  
                                                    {
                                                    this.state.isHovering && this.state.isItemHover === 3 
                                                    ?
                                                    <div>
                                                        <Flip right cascade when={this.state.toggleHoverState}>
                                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3> 
                                                        </Flip>
                                                        <Flip bottom cascade when={this.state.show}>
                                                        <Link to="/portfolio/item" className="primary-btn white-color  white-shadow">View</Link>
                                                        </Flip>
                                                    </div>
                                                    : ''
                                                    }
                                                </div>
                                            </Link>
                                        </div>
                                    </Zoom>
                                    
                                    <Zoom delay={600}>
                                        <div className="card bg-light text-white items" data-item="4" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                                            <Link to="/portfolio/item" className="plain-link2">
                                                <img src="/img/saitama.jpg" className="card-img-top" alt="..."  style={{width: '366px', height: '366px'}} />
                                                <div className="card-img-overlay text-center"> 
                                                    {
                                                    this.state.isHovering && this.state.isItemHover === 4 
                                                    ?
                                                    <div>
                                                        <Flip right cascade when={this.state.toggleHoverState}>
                                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3> 
                                                        </Flip>
                                                        <Flip bottom cascade when={this.state.show}>
                                                        <Link to="/portfolio/item" className="primary-btn white-color  white-shadow">View</Link>
                                                        </Flip>
                                                    </div>
                                                    : ''
                                                    }
                                                </div>
                                            </Link>
                                        </div>
                                    </Zoom>
                                    
                                    <Zoom delay={800}>
                                        <div className="card bg-light text-white items" data-item="5" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                                            <Link to="/portfolio/item" className="plain-link2">
                                                <img src="/img/bg1.jpg" className="card-img-top" alt="..."  style={{width: '366px', height: '366px'}} />
                                                <div className="card-img-overlay text-center"> 
                                                {
                                                    this.state.isHovering && this.state.isItemHover === 5 
                                                    ?
                                                    <div>
                                                        <Flip right cascade when={this.state.toggleHoverState}>
                                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3> 
                                                        </Flip>
                                                        <Flip bottom cascade when={this.state.show}>
                                                        <Link to="/portfolio/item"  className="primary-btn white-color  white-shadow">View</Link>
                                                        </Flip>
                                                    </div>
                                                    : ''
                                                    }
                                                </div>
                                            </Link>
                                        </div>
                                    </Zoom>
                                    
                                    <Zoom delay={1000}>
                                        <div className="card bg-light text-white items" data-item="6" onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                                            <Link to="/portfolio/item" className="plain-link2">
                                                <img src="/img/bg3.jpg" className="card-img-top" alt="..."  style={{width: '366px', height: '366px'}} />
                                                <div className="card-img-overlay text-center"> 
                                                    {
                                                    this.state.isHovering && this.state.isItemHover === 6 
                                                    ?
                                                    <div>
                                                        <Flip right cascade when={this.state.toggleHoverState}>
                                                        <h3 className="card-title white-shadow title">This is a Sample Title</h3> 
                                                        </Flip>
                                                        <Flip bottom cascade when={this.state.show}>
                                                        <Link to="/portfolio/item"  className="primary-btn white-color  white-shadow">View</Link>
                                                        </Flip>
                                                    </div>
                                                    : ''
                                                    }
                                                </div>
                                            </Link>
                                        </div>
                                    </Zoom>

                                </div>
                                :
                                <div>
                                    <div className="card-columns">
                                    {
                                        this.state.work.map(item => 
                                        <Zoom delay="100">
                                            <div className="card bg-light text-white items" data-item={item.pk} onMouseEnter={this.handleMouseHover} onMouseLeave={this.handleMouseHover}>
                                                <Link to={`/portfolio/`+item.pk} className="plain-link2">
                                                    <img src={this.state.site.media_url+``+item.fields.cover_image} className="card-img-top" alt={item.fields.title}  style={{width: '366px', height: '366px'}} />
                                                    <div className="card-img-overlay text-center"> 
                                                        {
                                                            this.state.isHovering && this.state.isItemHover === item.pk
                                                            ?
                                                            <div>
                                                                <Flip right cascade when={this.state.toggleHoverState}>
                                                                <h3 className="card-title white-shadow title">{item.fields.title}</h3> 
                                                                </Flip>
                                                                <Flip bottom cascade when={this.state.show}>
                                                                <Link to={`/portfolio/`+item.pk} className="primary-btn white-color  white-shadow">View</Link>
                                                                </Flip>
                                                            </div>
                                                            : ''
                                                        }
                                                    </div>
                                                </Link>
                                            </div>
                                        </Zoom>
                                        )
                                    }
                                    </div>
                                    <div className="row justify-content-center">
                                        <hr />
                                        <LightSpeed bottom>
                                            <BlogPagination pages={this.state.pages} history={this.props.history} active={active}  />
                                        </LightSpeed>
                                    </div>
                                </div>
                        }
                    </div>
                </div>
            </section>
        );
    }
}

export default Portfolio;
import React from  'react';

class Social extends React.Component {
    render() {
        return (
            this.props.social.length === 0 ?
            <ul className="list">
                <li><a className="plain-link" target="_blank" rel="noopener noreferrer" href="/"><i className="fa fa-facebook"></i></a></li>
                <li><a className="plain-link" target="_blank" rel="noopener noreferrer" href="/"><i className="fa fa-google-plus"></i></a></li>
                <li><a className="plain-link" target="_blank" rel="noopener noreferrer" href="/"><i className="fa fa-twitter"></i></a></li>
                <li><a className="plain-link" target="_blank" rel="noopener noreferrer" href="/"><i className="fa fa-youtube"></i></a></li>
            </ul>
            :
            <ul className="list">
                {
                    this.props.social.map(item=>
                    <li>
                        <a className="plain-link" target="_blank" rel="noopener noreferrer" href={item.fields.link}><i className={item.fields.icon}></i></a>
                    </li>
                    )
                }
            </ul>
        )
    }
}

export default Social;
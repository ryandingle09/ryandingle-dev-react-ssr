import React from 'react';
import Social from '../global/social';
import { Bounce } from 'react-reveal';

class Footer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            site: [this.props.site][0]
        };
    }

    render() {

        return (
            <footer className="footer_area section_gap">
                <Bounce>
                <div className="container">
                    <div className="row footer_inner justify-content-center">
                        {
                            this.props.meta.length === 0 ?
                                <div className="col-lg-6 text-center">
                                    <aside className="f_widget social_widget">
                                        <div className="f_logo">
                                            <img src="/img/logo.png" alt="" />
                                        </div>
                                        <div className="f_title">
                                            <h4>Follow Me</h4>
                                        </div>
                                        <Social social={this.props.social} />
                                    </aside>
                                    <div className="copyright">
                                        <p>
                                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i className="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://rdingle.com" target="_blank" rel="noopener noreferrer">RLD WEBSHOP</a>
                                        </p>
                                    </div>
                                </div>
                            :
                            this.props.meta.map(item => 
                                <div className="col-lg-6 text-center">
                                    <aside className="f_widget social_widget">
                                        <div className="f_logo">
                                            {
                                                item.fields.use_logo ?
                                                <img src={this.state.site.media_url+``+item.fields.logo} alt={item.fields.title} />
                                                :
                                                <h1 style={{color: 'white'}}>{item.fields.title}</h1>
                                            }
                                        </div>
                                        <div className="f_title">
                                            <h4>Follow Me</h4>
                                        </div>
                                        <Social social={this.props.social} />
                                    </aside>
                                    <div className="copyright">
                                        <p>
                                            Copyright &copy;{item.fields.year} All rights reserved <i className="fa fa-star-o" aria-hidden="true"></i> by <a href={`https://`+item.fields.domain_name} target="_blank" rel="noopener noreferrer">{item.fields.title}</a>
                                        </p>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
                </Bounce>
            </footer>
        );
    }
}

export default Footer;
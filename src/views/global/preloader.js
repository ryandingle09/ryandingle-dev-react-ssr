import React from 'react';
import ReactLoading from "react-loading";

const Preloader = (props) => {
    return (
        <h1 className="mt-s text-center mb-s">
            <div style={{width: '65px', margin: '0px auto'}}>
                <ReactLoading type="spin" color="#ccc" />
            </div>
        </h1>
    );
}

export default Preloader;


import React from 'react';
// import { graphql } from "gatsby-graphql";
import { DiscussionEmbed } from "disqus-react"

const DisqusComments = (props) => {
    const disqusConfig = {
        shortname: process.env.REACT_APP_GATSBY_DISQUS_NAME,
        config: { identifier: props.item }
    }

    return (
        <DiscussionEmbed {...disqusConfig} />
    );
};

export default DisqusComments;
import React from "react";
import { Helmet } from "react-helmet";

class Meta extends React.Component{

    render() {
        return(
            <div>
            {
                <Helmet>
                    <title>{this.props.title ? this.props.title : 'Ryan Dingle'}</title>
                    
                    {/* Essential META Tags */}
                    <meta property="og:title" content={this.props.item_title ? this.props.item_title : 'Ryan Dingle Personal Website'}  />
                    <meta property="og:description" content={this.props.item_description ? this.props.item_description : 'Welcome to My Personal Website'} />
                    <meta property="og:image" content={this.props.item_image ? this.props.item_image : window.location.protocol+ `://`+ window.location.host +`/img/bg3.jpg`} />
                    <meta property="og:url" content={this.props.item_url ? this.props.item_url : window.location.href } />
                    <meta name="twitter:card" content={this.props.item_image ? this.props.item_image : window.location.protocol+ `://`+ window.location.host +`/img/bg3.jpg`} />

                    {/*  Non-Essential, But Recommended  */}
                    <meta property="og:site_name" content={this.props.title ? this.props.title : 'Ryan Dingle'} />
                    <meta name="twitter:image:alt" content={this.props.item_title ? this.props.item_title : 'Ryan Dingle Personal Website'} />

                    {/* Non-Essential, But Required for Analytics */}
                    <meta property="fb:app_id" content="265900324300424" />
                    <meta name="twitter:site" content="@ryandingle09" />
                </Helmet>
            }
            </div>
        );
    }
}

export default Meta;
import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import { createMemoryHistory } from 'history';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';

import Routes from "./Routes";
import Header from './views/global/header';
import Footer from './views/global/footer';
import SiteData from './views/utils/site-data';
import Preloader from './views/global/preloader';
import API from './views/global/api';

class App extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      isLoading: true,
      meta: [],
      work: [],
      about: [],
      blog: [],
      social: [],
    }
  }

  componentDidMount() {
    this.getData();
  }
  
  getData() {
    API.get('site')
        .then(res => this.setState({
                meta: res.data,
                isLoading: false,
            })
        )
        .catch(error => this.setState({
                isLoading: false
            })
        )
        .catch(error => {
            alert('Something went wrong. Please try again later.');
            console.log(error);
        });

    API.get('social')
        .then(res => this.setState({
                social: res.data,
                isLoading: false,
            })
        )
        .catch(error => this.setState({
                isLoading: false
            })
        )
        .catch(error => {
            alert('Something went wrong. Please try again later.');
            console.log(error);
        });
  }
  
  render() {
    const history = createMemoryHistory();
    return (
      <div>
          { this.state.isLoading ?
            <Preloader />
          :
            <Router hitory={history}>
              <ToastContainer position={toast.POSITION.BOTTOM_CENTER} autoClose={false} />
              <Header site={SiteData} meta={this.state.meta} />
              <Routes isLoading={this.state.isLoading} meta={this.state.meta} />
              <Footer site={SiteData} meta={this.state.meta} social={this.state.social} />
            </Router>
          }
      </div>
    );
  }
}

export default App;

import React from "react";
import { Route, Switch } from "react-router-dom";

import Home from "./views/pages/home/home";
import About from "./views/pages/about/about";
import Blog from "./views/pages/blog/blog";
import BlogItem from "./views/pages/blog/blogItem";
import BlogCategory from "./views/pages/blog/blogCategory";
import BlogTag from "./views/pages/blog/blogTag";
import Portfolio from "./views/pages/portfolio/portfolio";
import PortfolioItem from "./views/pages/portfolio/portfolioItem";
import Contact from "./views/pages/contact/contact";
import Services from "./views/pages/services/services";
import NotFound from './views/pages/404';
import SiteData from './views/utils/site-data';

class Routes extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact={true}  path="/" render={(props) => <Home {...props} site={SiteData} isLoading={this.props.isLoading} meta={this.props.meta} />} />
        <Route exact={true}  path="/about" render={(props) => <About {...props} site={SiteData} />} />
        <Route exact={true}  path="/contact" render={(props) => <Contact {...props} site={SiteData} />} />
        <Route exact={true}  path="/portfolio" render={(props) => <Portfolio {...props} site={SiteData} />} />
        <Route exact={true}  path="/portfolio/:id" render={(props) => <PortfolioItem {...props} site={SiteData} />} />
        <Route exact={true}  path="/blog" render={(props) => <Blog {...props} site={SiteData} />} />
        <Route exact={true}  path="/services" render={(props) => <Services {...props} site={SiteData} />} />
        <Route exact={true}  path="/blog/:slug" render={(props) => <BlogItem {...props} site={SiteData} />} />
        <Route exact={true}  path="/blog/category/:id" render={(props) => <BlogCategory {...props} site={SiteData} />} />
        <Route exact={true}  path="/blog/tag/:id" render={(props) => <BlogTag {...props} site={SiteData} />} />
        <Route
          render={function () {
            return <NotFound site={SiteData}  />;
          }}
        />
      </Switch>
    );
  }
}

export default Routes;
